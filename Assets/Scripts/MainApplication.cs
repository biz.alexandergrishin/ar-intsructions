﻿using Controller;
using Model;
using UnityEngine;

// Base class for all elements in this application.
public class MainElement : MonoBehaviour
{
    // Gives access to the application and all instances.
    public MainApplication app { get { return GameObject.FindObjectOfType<MainApplication>(); } }
}

public class MainApplication : MainElement
{
    // Reference to the root instances of the MVC.
    public MainModel model;
    public MainView view;
    public MainController controller;

    // Init things here
    void Start() { }

    // Iterates all Controllers and delegates the notification data
    // This method can easily be found because every class is “BounceElement” and has an “app” 
    // instance.
    public void Notify(string p_event_path, Object p_target, params string[] p_data)
    {
        MainController[] controller_list = GetAllControllers();
        foreach (MainController c in controller_list)
        {
            c.OnNotification(p_event_path, p_target, p_data);
        }
    }

    // Fetches all scene Controllers.
    public MainController[] GetAllControllers() { return FindObjectsOfType<MainController>(); }
}
