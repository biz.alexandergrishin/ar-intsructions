﻿using System;
using static Model.MainModel;

class MainNotifications : MainElement
{

    /// <summary>
    /// Notifications for buttons
    /// </summary>
    public enum Notifications
    {
        chooseItem1,
        instExp1Next,
        instExp1Prev,
        reset,
        finish
    }

    /* Main Game Notifications */
    public static readonly string game_start = "game.start";
    /* Notifications */
    
    /* Button Notifications */
    public static readonly string chooseItem1 = "chooseItem1";
    
    /* Navigation/Transition Notifications */
    public static string instExp1Start = "instructions.exp.1.start";
    public static string instExp1Next = "instExp1Next";
    public static string instExp1Prev = "instExp1Prev";
    public static string reset = "reset";
    public static string finish = "finish";

}
