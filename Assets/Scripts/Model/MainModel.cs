﻿using UnityEngine;

namespace Model
{
    public class MainModel : MonoBehaviour
    {
        public Vector3 spawnPosition;
        public bool startSpawn = false;

        public float speed = 0.5f;
    }
}
