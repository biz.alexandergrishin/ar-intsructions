﻿using UnityEngine;

namespace Controller
{
    public class AnimationController : MainElement
    {
        Vector3 step1EndPosition;
        
        // Start is called before the first frame update
        void Start()
        {   
            
        }

        // Update is called once per frame
        void Update()
        {
            //Animation for Step#1
            if (!app.view.step1BottomPart.transform.parent.gameObject.activeSelf) return;
            var position = app.view.step1BottomPart.transform.position;
            var startPosition = new Vector3(position.x, position.y , position.z);
                
            if (step1EndPosition == Vector3.zero)
            {
                step1EndPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z + 25);
            }

//                if (app.view.step1BottomPart.transform.position == step1EndPosition)
//                {
//                    step1EndPosition = position;
//                    startPosition = app.view.step1BottomPart.transform.position;
//                }

            app.view.step1BottomPart.transform.position = Vector3.MoveTowards(startPosition, step1EndPosition, app.model.speed * Time.deltaTime);
        }
    }
}
