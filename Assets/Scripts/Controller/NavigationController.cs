﻿using UnityEngine;

namespace Controller
{
    public class NavigationController : MainElement
    {
        /// <summary>
        /// Set correct states for each object 
        /// </summary>
        public void Start()
        {
            Finish();
        }
    
        public void StartARSession()
        {
            app.view.arMenu.SetActive(true);
            app.view.mainMenu.SetActive(false);
        }

        public void StartExp1()
        {   
            app.view.prepareBottom.gameObject.SetActive(false);
            app.view.bottomItems[0].SetActive((true));
            app.view.shelvesObject[0].SetActive((true));
        }

        public void NextStep()
        {
            for (var i = 0; i < app.view.shelvesObject.Length; i++)
            {
                if (!app.view.shelvesObject[i].activeSelf) continue;
                app.view.shelvesObject[i].SetActive(false);
                app.view.shelvesObject[i+1].SetActive((true));
            
                app.view.bottomItems[i].SetActive(false);
                app.view.bottomItems[i+1].SetActive((true));
                return;
            }
        }
    
        public void PrevStep()
        {
            for (var i = 0; i < app.view.shelvesObject.Length; i++)
            {
                if (!app.view.shelvesObject[i].activeSelf) continue;
                app.view.shelvesObject[i].SetActive(false);
                app.view.shelvesObject[i-1].SetActive((true));
            
                app.view.bottomItems[i].SetActive(false);
                app.view.bottomItems[i-1].SetActive((true));
                return;
            }
        }

        public void Reset()
        {
            for (int i = 0; i < app.view.shelvesObject.Length; i++)
            {
                app.view.shelvesObject[i].SetActive(false);
            }
        
            for (int i = 0; i < app.view.bottomItems.Length; i++)
            {
                app.view.bottomItems[i].SetActive(false);
            }
        
            app.view.prepareBottom.SetActive((true));
        
            app.model.spawnPosition = Vector3.zero;
        }

        public void Finish()
        {
            app.view.mainMenu.SetActive((true));
            app.view.arMenu.SetActive(false);

            Reset();

            app.model.startSpawn = false;
        }
    }
}
