﻿using UnityEngine;

namespace Controller
{
    public class MainController : MainElement
    {
        // Start is called before the first frame update
        [SerializeField] private NavigationController navigationController;
        [SerializeField] private PlaceObjectController placeObjectController;
       
        // Handles events here
        public void OnNotification(string p_event_path, object p_target, params string[] p_data)
        {
            //Handle Main Notifications 
            if (p_event_path == MainNotifications.game_start)
            {
            }

            if (p_event_path == MainNotifications.chooseItem1)
            {
                navigationController.StartARSession();
                app.model.startSpawn = true;
            }

            if (p_event_path == MainNotifications.instExp1Start)
            {
                navigationController.StartExp1();
                SetItemPositions();
            }

            if (p_event_path == MainNotifications.instExp1Next)
            {
                navigationController.NextStep();
            }
            
            if (p_event_path == MainNotifications.instExp1Prev)
            {
                navigationController.PrevStep();
            }
            
            if (p_event_path == MainNotifications.reset)
            {
                navigationController.Reset();
            }
            
            if (p_event_path == MainNotifications.finish)
            {
                navigationController.Finish();
            }
        }

        void SetItemPositions()
        {
            for (var i = 0; i < app.view.shelvesObject.Length; i++)
            {
                app.view.shelvesObject[i].transform.position = app.model.spawnPosition;
            }
        }
    }
}
