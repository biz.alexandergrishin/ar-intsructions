﻿using UnityEngine;

public class MainView : MainElement
{
    public GameObject[] shelvesObject;
    
    public Camera arCamera;

    public Collider editorPlane;
    
    public GameObject mainMenu;
    public GameObject arMenu;

    public GameObject prepareBottom;
    public GameObject[] bottomItems;
    
    
    [Header("Animation Parts")]
    //Item Part for Animation
    // In production all animation can be implemented directly in 3D model
    public GameObject step1BottomPart;
    
}
