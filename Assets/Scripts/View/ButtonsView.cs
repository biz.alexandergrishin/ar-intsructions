﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsView : MainElement
{
    [SerializeField]
    private MainNotifications.Notifications notification;
    private void Start()
    {
        var notificationString = Enum.GetName(typeof(MainNotifications.Notifications), notification);
        GetComponent<Button>().onClick.AddListener(delegate { app.Notify(notificationString, this); });
    }
    
    
}
